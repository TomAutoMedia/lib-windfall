var wf = (()=>{
    
    let base = new class{};
    
    /**
     * Selects an element from either the scope or the document
     * @param   {string} rules css selector rules
     * @param   {HTMLElement,Null} scope an HTMLElement object to work from or defaults to document 
     * @returns {HTMLElement} the first found htmlelement which matched the rules
     */
    base.qs = function qs(rules,scope){
        if(scope !== undefined)
            {
                return scope.querySelector(rules);
            }
        else 
            {
                return document.querySelector(rules);
            }
    };
    
    base.n =  new class{
        
        /**
         * Creates an element and automatically applies attributes
         * @param   {String} tag        String containing the name of an HTML Tag to create
         * @param   {Object} attributes optional object with attributes to apply to the element
         * @returns {HTMLElement} returns a copy of the element that was created
         */
        createElement(tag,attributes){
            let out =  document.createElement(tag);
            
            if(attributes !== null && attributes !== undefined)
            Object.assign(out,attributes);
            
            return out;
        }
        
        createText(text)
        {
            return document.createTextNode(text);
        }
        
        /**
         * Creates a new cell for a row in a table and automatically applies attributes
         * @param   {HTMLTableRowElement} row row to create new cell in
         * @param   {Object} attributes Optional object with attribute to apply to the cell element
         * @returns {HTMLTableRowCellElement} returns a copy of the cell element created
         */
        insertCell(row,attributes){
            let out =  row.insertCell();
            
            if(attributes !== null && attributes !== undefined)
            Object.assign(out,attributes);
            
            return out;
        }
        
        /**
         * Inserts the element or node as the last child of the target
         * @param {HTMLElement} target        target HTMLElement to append to
         * @param {HTMLElement | Node} elementOrNode Element or Node object to append
         */
        append(target,elementOrNode){
            if(elementOrNode instanceof Node)
                {
                    target.appendChild(elementOrNode);
                }
            else if(elementOrNode instanceof HTMLElement)
                {
                    target.insertAdjacentElement('beforeend',elementOrNode);
                }
            return target;
        }
        
        
    }
    
    return base;
    
})();